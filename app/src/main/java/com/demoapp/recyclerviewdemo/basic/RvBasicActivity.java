package com.demoapp.recyclerviewdemo.basic;

import android.os.Bundle;

import com.demoapp.recyclerviewdemo.R;
import com.demoapp.recyclerviewdemo.databinding.ActivityRecyclerviewBinding;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

public class RvBasicActivity extends AppCompatActivity {

    private ActivityRecyclerviewBinding mViewBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Init view binding.
        mViewBinding = ActivityRecyclerviewBinding.inflate(getLayoutInflater());

        setContentView(mViewBinding.getRoot());

        // Init fragment.
        FragmentManager fm = getSupportFragmentManager();

        if (fm.findFragmentByTag(RvBasicFragment.TAG) == null) {
            fm.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, RvBasicFragment.newInstance(), RvBasicFragment.TAG)
                    .commit();
        }
    }
}
