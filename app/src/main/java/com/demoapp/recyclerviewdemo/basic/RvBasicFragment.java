package com.demoapp.recyclerviewdemo.basic;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.demoapp.recyclerviewdemo.R;
import com.demoapp.recyclerviewdemo.UserDataModel;
import com.demoapp.recyclerviewdemo.databinding.FragmentRecyclerviewBinding;
import com.demoapp.recyclerviewdemo.utils.SampleUserData;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

public class RvBasicFragment extends Fragment implements RvBasicAdapter.Callback {

    public static final String TAG = RvBasicFragment.class.getSimpleName();

    private FragmentRecyclerviewBinding mViewBinding;
    private RvBasicAdapter mAdapter;
    private List<UserDataModel> mData;

    public RvBasicFragment() {
    }

    public static RvBasicFragment newInstance() {
        return new RvBasicFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Init view binding.
        mViewBinding = FragmentRecyclerviewBinding.inflate(inflater, container, false);

        return mViewBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Init adapter.
        mAdapter = new RvBasicAdapter();
        mAdapter.setCallback(this);

        // Init recycler view.
        mViewBinding.recyclerview.setLayoutManager(new LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false));
        mViewBinding.recyclerview.setHasFixedSize(false);
        mViewBinding.recyclerview.setAdapter(mAdapter);

        // Load adapter data.
        loadAdapterData();

        // Init view.
        if (mData != null && mData.size() > 0) {
            mViewBinding.recyclerview.setVisibility(View.VISIBLE);
            mViewBinding.textviewNoData.setVisibility(View.GONE);
        } else {
            mViewBinding.recyclerview.setVisibility(View.GONE);
            mViewBinding.textviewNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_recyclerview, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.menu_delete) {
            // Delete selected items. Options 1.
//            for (int i = mData.size() - 1; i >= 0; i--) {
//                if (mData.get(i).isSelected) {
//                    mData.remove(i);
//                }
//            }

            // Delete selected items. Options 2.
            for (UserDataModel userDataModel : getSelectedItem()) {
                mData.remove(userDataModel);
            }

            mAdapter.setData(mData);
            mAdapter.notifyDataSetChanged();

            Toast.makeText(requireActivity(), "Items deleted.", Toast.LENGTH_SHORT).show();

            return true;
        } else if (itemId == R.id.menu_refresh) {
            loadAdapterData();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemLongClicked(UserDataModel data) {
        for (UserDataModel userDataModel : mData) {
            if (userDataModel.id == data.id) {
                userDataModel.isSelected = !userDataModel.isSelected; // Toggle selection.

                break;
            }
        }

        Toast.makeText(requireActivity(), "Total selected items: " + getSelectedItem().size(), Toast.LENGTH_SHORT).show();

        mAdapter.notifyDataSetChanged();
    }

    private void loadAdapterData() {
        SampleUserData sampleUserData = new SampleUserData();

        mData = sampleUserData.getSampleUserData();

        mAdapter.setData(mData);
        mAdapter.notifyDataSetChanged();
    }

    private List<UserDataModel> getSelectedItem() {
        List<UserDataModel> selectedItemList = new ArrayList<>();

        for (UserDataModel userDataModel : mData) {
            if (userDataModel.isSelected) {
                selectedItemList.add(userDataModel);
            }
        }

        return selectedItemList;
    }
}
