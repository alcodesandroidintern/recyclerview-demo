package com.demoapp.recyclerviewdemo.utils;

import com.demoapp.recyclerviewdemo.UserDataModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class SampleUserData {

    // JSON data generated from: https://www.json-generator.com/
    // JSON formatted from: https://jsonformatter.curiousconcept.com/
    private final String mSampleJson = "{data:[{\"id\":0,\"name\":\"Macias Small\",\"email\":\"maciassmall@ovolo.com\"},{\"id\":1,\"name\":\"Ava Walls\",\"email\":\"avawalls@ovolo.com\"},{\"id\":2,\"name\":\"Berger Henson\",\"email\":\"bergerhenson@ovolo.com\"},{\"id\":3,\"name\":\"Wheeler Cooper\",\"email\":\"wheelercooper@ovolo.com\"},{\"id\":4,\"name\":\"Bobbi Gray\",\"email\":\"bobbigray@ovolo.com\"},{\"id\":5,\"name\":\"Hernandez Ashley\",\"email\":\"hernandezashley@ovolo.com\"},{\"id\":6,\"name\":\"Mollie Sullivan\",\"email\":\"molliesullivan@ovolo.com\"},{\"id\":7,\"name\":\"Janie Nichols\",\"email\":\"janienichols@ovolo.com\"},{\"id\":8,\"name\":\"Christian Williams\",\"email\":\"christianwilliams@ovolo.com\"},{\"id\":9,\"name\":\"Melissa Burch\",\"email\":\"melissaburch@ovolo.com\"},{\"id\":10,\"name\":\"Walters Williamson\",\"email\":\"walterswilliamson@ovolo.com\"},{\"id\":11,\"name\":\"Bridgett Gentry\",\"email\":\"bridgettgentry@ovolo.com\"},{\"id\":12,\"name\":\"Cooke Russo\",\"email\":\"cookerusso@ovolo.com\"},{\"id\":13,\"name\":\"Gordon York\",\"email\":\"gordonyork@ovolo.com\"},{\"id\":14,\"name\":\"Kemp Quinn\",\"email\":\"kempquinn@ovolo.com\"},{\"id\":15,\"name\":\"Tanya Poole\",\"email\":\"tanyapoole@ovolo.com\"},{\"id\":16,\"name\":\"Jacklyn Gibson\",\"email\":\"jacklyngibson@ovolo.com\"},{\"id\":17,\"name\":\"Atkinson Sims\",\"email\":\"atkinsonsims@ovolo.com\"},{\"id\":18,\"name\":\"Iris Malone\",\"email\":\"irismalone@ovolo.com\"},{\"id\":19,\"name\":\"Inez Beach\",\"email\":\"inezbeach@ovolo.com\"},{\"id\":20,\"name\":\"Rae Holland\",\"email\":\"raeholland@ovolo.com\"},{\"id\":21,\"name\":\"Cote Abbott\",\"email\":\"coteabbott@ovolo.com\"},{\"id\":22,\"name\":\"Stacie Thompson\",\"email\":\"staciethompson@ovolo.com\"},{\"id\":23,\"name\":\"Arlene Wiley\",\"email\":\"arlenewiley@ovolo.com\"},{\"id\":24,\"name\":\"Lisa Steele\",\"email\":\"lisasteele@ovolo.com\"},{\"id\":25,\"name\":\"Zelma Johnson\",\"email\":\"zelmajohnson@ovolo.com\"},{\"id\":26,\"name\":\"Estrada Hardin\",\"email\":\"estradahardin@ovolo.com\"},{\"id\":27,\"name\":\"Mccarty Dalton\",\"email\":\"mccartydalton@ovolo.com\"},{\"id\":28,\"name\":\"Harris Acosta\",\"email\":\"harrisacosta@ovolo.com\"},{\"id\":29,\"name\":\"Janet Howard\",\"email\":\"janethoward@ovolo.com\"},{\"id\":30,\"name\":\"Morrison Simmons\",\"email\":\"morrisonsimmons@ovolo.com\"},{\"id\":31,\"name\":\"Chapman Combs\",\"email\":\"chapmancombs@ovolo.com\"},{\"id\":32,\"name\":\"Parsons Strickland\",\"email\":\"parsonsstrickland@ovolo.com\"},{\"id\":33,\"name\":\"Hester Alexander\",\"email\":\"hesteralexander@ovolo.com\"},{\"id\":34,\"name\":\"Potts Sherman\",\"email\":\"pottssherman@ovolo.com\"},{\"id\":35,\"name\":\"Torres Gould\",\"email\":\"torresgould@ovolo.com\"},{\"id\":36,\"name\":\"White Rowland\",\"email\":\"whiterowland@ovolo.com\"},{\"id\":37,\"name\":\"Schwartz Glover\",\"email\":\"schwartzglover@ovolo.com\"},{\"id\":38,\"name\":\"Latasha Bryan\",\"email\":\"latashabryan@ovolo.com\"},{\"id\":39,\"name\":\"Manuela Herring\",\"email\":\"manuelaherring@ovolo.com\"},{\"id\":40,\"name\":\"Elnora Lloyd\",\"email\":\"elnoralloyd@ovolo.com\"},{\"id\":41,\"name\":\"Carole Barton\",\"email\":\"carolebarton@ovolo.com\"},{\"id\":42,\"name\":\"Pennington Meyers\",\"email\":\"penningtonmeyers@ovolo.com\"},{\"id\":43,\"name\":\"Nellie Hatfield\",\"email\":\"nelliehatfield@ovolo.com\"},{\"id\":44,\"name\":\"Lara Mccarthy\",\"email\":\"laramccarthy@ovolo.com\"},{\"id\":45,\"name\":\"Della Decker\",\"email\":\"delladecker@ovolo.com\"},{\"id\":46,\"name\":\"Shannon Lester\",\"email\":\"shannonlester@ovolo.com\"},{\"id\":47,\"name\":\"Tran Clarke\",\"email\":\"tranclarke@ovolo.com\"},{\"id\":48,\"name\":\"Klein Snyder\",\"email\":\"kleinsnyder@ovolo.com\"},{\"id\":49,\"name\":\"Rush Garcia\",\"email\":\"rushgarcia@ovolo.com\"}]}";

    public List<UserDataModel> getSampleUserData() {
        // Simulate get data from API or other source providers.
        List<UserDataModel> userDataModelList = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(mSampleJson);
            JSONArray jsonArray = jsonObject.getJSONArray("data");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jobj = jsonArray.getJSONObject(i);
                UserDataModel model = new UserDataModel();

                model.id = jobj.getInt("id");
                model.fullName = jobj.getString("name");
                model.email = jobj.getString("email");
                model.isSelected = false;

                userDataModelList.add(model);
            }
        } catch (JSONException e) {
            Timber.e(e);
        }

        return userDataModelList;
    }
}
