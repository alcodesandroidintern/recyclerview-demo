package com.demoapp.recyclerviewdemo;

public class UserDataModel {

    public int id;
    public String fullName;
    public String email;

    public boolean isSelected; // For recyclerview selection.
}
