package com.demoapp.recyclerviewdemo;

import android.os.Bundle;

import com.demoapp.recyclerviewdemo.databinding.ActivityMainBinding;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding mViewBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Init view binding.
        mViewBinding = ActivityMainBinding.inflate(getLayoutInflater());

        setContentView(mViewBinding.getRoot());

        // Init fragment.
        FragmentManager fm = getSupportFragmentManager();

        if (fm.findFragmentByTag(MainFragment.TAG) == null) {
            fm.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, MainFragment.newInstance(), MainFragment.TAG)
                    .commit();
        }
    }
}
