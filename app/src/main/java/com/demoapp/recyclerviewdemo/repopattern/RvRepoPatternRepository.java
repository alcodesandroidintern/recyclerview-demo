package com.demoapp.recyclerviewdemo.repopattern;

import com.demoapp.recyclerviewdemo.UserDataModel;
import com.demoapp.recyclerviewdemo.utils.SampleUserData;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class RvRepoPatternRepository {

    private static RvRepoPatternRepository mInstance;

    private MutableLiveData<List<UserDataModel>> mUserDataModelLiveData = new MutableLiveData<>();

    private RvRepoPatternRepository() {
    }

    public static RvRepoPatternRepository getInstance() {
        if (mInstance == null) {
            synchronized (RvRepoPatternRepository.class) {
                if (mInstance == null) {
                    mInstance = new RvRepoPatternRepository();
                }
            }
        }

        return mInstance;
    }

    public LiveData<List<UserDataModel>> getUserDataModelLiveData() {
        return mUserDataModelLiveData;
    }

    public void loadUserData() {
        SampleUserData sampleUserData = new SampleUserData();

        mUserDataModelLiveData.setValue(sampleUserData.getSampleUserData());
    }

    public void setSelectedItem(int id) {
        List<UserDataModel> data = mUserDataModelLiveData.getValue();
        boolean itemFound = false;

        for (UserDataModel userDataModel : data) {
            if (userDataModel.id == id) {
                userDataModel.isSelected = !userDataModel.isSelected; // Toggle selection.
                itemFound = true;

                break;
            }
        }

        if (itemFound) {
            // Only notify update if list is changed.
            mUserDataModelLiveData.setValue(data);
        }
    }

    public List<UserDataModel> getSelectedItems() {
        List<UserDataModel> data = mUserDataModelLiveData.getValue();
        List<UserDataModel> selectedItemList = new ArrayList<>();

        for (UserDataModel userDataModel : data) {
            if (userDataModel.isSelected) {
                selectedItemList.add(userDataModel);
            }
        }

        return selectedItemList;
    }

    public void deleteSelectedItems() {
        List<UserDataModel> data = mUserDataModelLiveData.getValue();
        boolean hasItemDeleted = false;

        for (int i = data.size() - 1; i >= 0; i--) {
            if (data.get(i).isSelected) {
                data.remove(i);
                hasItemDeleted = true;
            }
        }

        if (hasItemDeleted) {
            // Only notify update if list is changed.
            mUserDataModelLiveData.setValue(data);
        }
    }
}
