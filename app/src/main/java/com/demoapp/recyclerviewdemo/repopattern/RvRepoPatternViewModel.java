package com.demoapp.recyclerviewdemo.repopattern;

import android.app.Application;

import com.demoapp.recyclerviewdemo.UserDataModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class RvRepoPatternViewModel extends AndroidViewModel {

    private RvRepoPatternRepository mRepository;

    public RvRepoPatternViewModel(@NonNull Application application) {
        super(application);

        mRepository = RvRepoPatternRepository.getInstance();
        mRepository.loadUserData();
    }

    public LiveData<List<UserDataModel>> getUserDataModelLiveData() {
        return mRepository.getUserDataModelLiveData();
    }

    public void loadUserData() {
        mRepository.loadUserData();
    }

    public void setSelectedItem(int id) {
        mRepository.setSelectedItem(id);
    }

    public List<UserDataModel> getSelectedItem() {
        return mRepository.getSelectedItems();
    }

    public void deleteSelectedItems() {
        mRepository.deleteSelectedItems();
    }
}
