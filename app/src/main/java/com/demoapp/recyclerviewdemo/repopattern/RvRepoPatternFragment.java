package com.demoapp.recyclerviewdemo.repopattern;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.demoapp.recyclerviewdemo.R;
import com.demoapp.recyclerviewdemo.UserDataModel;
import com.demoapp.recyclerviewdemo.basic.RvBasicAdapter;
import com.demoapp.recyclerviewdemo.databinding.FragmentRecyclerviewBinding;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

public class RvRepoPatternFragment extends Fragment implements RvBasicAdapter.Callback {

    public static final String TAG = RvRepoPatternFragment.class.getSimpleName();

    private FragmentRecyclerviewBinding mViewBinding;
    private RvRepoPatternViewModel mViewModel;
    private RvBasicAdapter mAdapter;

    public RvRepoPatternFragment() {
    }

    public static RvRepoPatternFragment newInstance() {
        return new RvRepoPatternFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Init view binding.
        mViewBinding = FragmentRecyclerviewBinding.inflate(inflater, container, false);

        return mViewBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Init adapter.
        mAdapter = new RvBasicAdapter();
        mAdapter.setCallback(this);

        // Init recycler view.
        mViewBinding.recyclerview.setLayoutManager(new LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false));
        mViewBinding.recyclerview.setHasFixedSize(false);
        mViewBinding.recyclerview.setAdapter(mAdapter);

        // Init view model.
        mViewModel = new ViewModelProvider(
                this,
                ViewModelProvider.AndroidViewModelFactory.getInstance(requireActivity().getApplication())
        ).get(RvRepoPatternViewModel.class);

        mViewModel.getUserDataModelLiveData().observe(getViewLifecycleOwner(), new Observer<List<UserDataModel>>() {

            @Override
            public void onChanged(List<UserDataModel> userDataModels) {
                mAdapter.setData(userDataModels);
                mAdapter.notifyDataSetChanged();

                if (userDataModels != null && userDataModels.size() > 0) {
                    mViewBinding.recyclerview.setVisibility(View.VISIBLE);
                    mViewBinding.textviewNoData.setVisibility(View.GONE);

                    int selectedItemCount = mViewModel.getSelectedItem().size();

                    if (selectedItemCount > 0) {
                        Toast.makeText(requireActivity(), "Total selected items: " + selectedItemCount, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    mViewBinding.recyclerview.setVisibility(View.GONE);
                    mViewBinding.textviewNoData.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_recyclerview, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.menu_delete) {
            // Delete selected items.
            mViewModel.deleteSelectedItems();

            Toast.makeText(requireActivity(), "Items deleted.", Toast.LENGTH_SHORT).show();

            return true;
        } else if (itemId == R.id.menu_refresh) {
            mViewModel.loadUserData();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemLongClicked(UserDataModel data) {
        mViewModel.setSelectedItem(data.id);
    }
}
