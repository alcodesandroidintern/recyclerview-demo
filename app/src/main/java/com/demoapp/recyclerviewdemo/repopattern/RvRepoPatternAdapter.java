package com.demoapp.recyclerviewdemo.repopattern;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demoapp.recyclerviewdemo.R;
import com.demoapp.recyclerviewdemo.UserDataModel;
import com.demoapp.recyclerviewdemo.databinding.ItemRecyclerviewBinding;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class RvRepoPatternAdapter extends RecyclerView.Adapter<RvRepoPatternAdapter.ViewHolder> {

    private List<UserDataModel> mData = new ArrayList<>();
    private RvRepoPatternAdapter.Callback mCallback;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RvRepoPatternAdapter.ViewHolder(ItemRecyclerviewBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallback);
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    public void setData(List<UserDataModel> data) {
        mData = new ArrayList<>();

        if (data != null) {
            mData.addAll(data);
        }
    }

    public void setCallback(RvRepoPatternAdapter.Callback callback) {
        mCallback = callback;
    }

    static final class ViewHolder extends RecyclerView.ViewHolder {

        public ItemRecyclerviewBinding viewBinding;

        public ViewHolder(@NonNull ItemRecyclerviewBinding viewBinding) {
            super(viewBinding.getRoot());

            this.viewBinding = viewBinding;
        }

        public void bindTo(final UserDataModel data, final Callback callback) {
            if (data != null) {
                // Bind data to UI.
                viewBinding.textviewFullName.setText(data.fullName);
                viewBinding.textviewEmail.setText(data.email);
                viewBinding.linearlayoutRoot.setOnLongClickListener(new View.OnLongClickListener() {

                    @Override
                    public boolean onLongClick(View view) {
                        if (callback != null) {
                            callback.onListItemLongClicked(data);
                        }

                        return true;
                    }
                });

                // Set list item background color.
                viewBinding.linearlayoutRoot.setBackgroundColor(ContextCompat.getColor(viewBinding.linearlayoutRoot.getContext(), (data.isSelected ? R.color.list_background_color_selected : R.color.list_background_color)));
            } else {
                // Reset UI.
                viewBinding.textviewFullName.setText("");
                viewBinding.textviewEmail.setText("");
                viewBinding.linearlayoutRoot.setOnLongClickListener(null);
            }
        }
    }

    public interface Callback {

        void onListItemLongClicked(UserDataModel data);
    }
}
