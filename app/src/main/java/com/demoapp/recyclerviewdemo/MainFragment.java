package com.demoapp.recyclerviewdemo;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demoapp.recyclerviewdemo.basic.RvBasicActivity;
import com.demoapp.recyclerviewdemo.databinding.FragmentMainBinding;
import com.demoapp.recyclerviewdemo.repopattern.RvRepoPatternActivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class MainFragment extends Fragment {

    public static final String TAG = MainFragment.class.getSimpleName();

    private FragmentMainBinding mViewBinding;

    public MainFragment() {
    }

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Init view binding.
        mViewBinding = FragmentMainBinding.inflate(inflater, container, false);

        return mViewBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mViewBinding.buttonBasic.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                startActivity(new Intent(requireActivity(), RvBasicActivity.class));
            }
        });

        mViewBinding.buttonRepoPattern.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                startActivity(new Intent(requireActivity(), RvRepoPatternActivity.class));
            }
        });
    }
}
